#!/bin/bash
echo "THIS WHONIX SCRIPT IS NO LONGER SUGGESTED! Please press CTRL-C to exit this installer, and in future use: 'sudo apt-get-update-plus dist-upgrade' to update your Whonix templates."
echo
echo
echo "THIS SCRIPT WILL REBOOT YOUR SYSTEM! SAVE ANYTHING IMPORTANT BEFORE CONTINUING!"
echo
echo "While this script works for me, I cannot guarantee this will not break your system. You have been warned."
echo
read -p "Press enter to continue, or ctrl-c to cancel."
sudo cp whonix-update.sh /bin/update
sudo chmod +x /bin/update
sudo echo 'export PATH=$PATH:~/bin' >> ~/.bashrc
read -p "Install complete. Press enter to reboot."
sudo reboot
